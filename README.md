==============DISCLAIMER==============

The author can not be hold responsible if this code breaks either your vectrex, your computer or your will to live.


==============ABOUT==============

This project is part of the "Advanced C and Assembly Programming" course at Pforzheim University.Goal of the course is the develop a game for the 1982 vectrex gaming console. 
A complete list of all 2019 projects can be found here: http://it.hs-pforzheim.de/personen/johannsen/projektlabor/vectrex_2019/vectrex_gallery_2019.htm


==============SYNOPSIS==============

- You have entered the salty waters of black mesa bay, try to survive as long as possible!
- It is recommended not to steer into any object, whether it is an island, an enemy boat or even an enemy canon ball.


==============HOW TO PLAY==============

Use the joystick to steer your boat.