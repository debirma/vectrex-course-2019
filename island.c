// ***************************************************************************
// island
// ***************************************************************************

#include <vectrex.h>
#include "utils/collision.h"
#include "utils/sound.h"
#include "utils/utils.h"
#include "utils/vector.h"
#include "island.h"
#include "game.h"
#include "level.h"
#include "player.h"
#include "tunes.h"

// ---------------------------------------------------------------------------

struct island_t islands[] =
{
	{{ .status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0}, .cooldown = 0},
	{{ .status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0}, .cooldown = 0},
	{{ .status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0}, .cooldown = 0},
	{{ .status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0}, .cooldown = 0},
};

// ---------------------------------------------------------------------------

unsigned int current_islands = 4;

#undef SF
#define SF 24

const struct packet_t vectors_island[] =
{
	{MOVE, { 0 * SF,  -3 * SF}},
	{DRAW, {  1 * SF, 1 * SF}},
	{DRAW, {  0 * SF,  2 * SF}},
	{DRAW, { -1 * SF,  1 * SF}},
	//{DRAW, { -1 * SF, -1 * SF}},
	{STOP, { 0, 0}},
};

// ---------------------------------------------------------------------------

void draw_island(struct object_t* p)
{
	Reset0Ref();				// reset beam to center of screen
	dp_VIA_t1_cnt_lo = 0x7f;	// set scaling factor for positioning
	Moveto_d(p->y, p->x);		// move beam to object coordinates
	dp_VIA_t1_cnt_lo = 0x22;	// set scalinf factor for drawing
	Draw_VLp(&vectors_island);	// draw vector list
}

// ---------------------------------------------------------------------------

void move_island(struct island_t* p)
{	
	if(move_object(&p->eObject)) 
	{
		p->cooldown = (int) Random();
		//current_game.difficulty++;
	}
}
// ---------------------------------------------------------------------------

void check_island(struct object_t* p)
{
	if (check_collision(player.y, player.x, p->y, p->x, 23, 18))
	{
		play_explosion(&bang);
		player.status = DEAD;
	}
}

// ---------------------------------------------------------------------------

void init_island(void)
{
	for (unsigned int i = 0; i < current_islands; ++i)
	{
		init_object(&islands[i].eObject, 2, 1);
		islands[i].eObject.status = INACTIVE;
		islands[i].cooldown = (int) Random();
	}
}
// ---------------------------------------------------------------------------

void handle_islands(void)
{	
	for (unsigned int i = 0; i < current_islands; ++i)
	{	
		if (islands[i].eObject.status == ACTIVE)
		{
			draw_island(&islands[i].eObject);
			move_island(&islands[i]);
			check_island(&islands[i].eObject);
		}
		else 
		{
			if(islands[i].cooldown > 0) islands[i].cooldown--;
			else islands[i].eObject.status = ACTIVE;
		}
	}
}

// ---------------------------------------------------------------------------

 /*
void addIsland(void)
{
	if(current_enemies < MAX_ENEMIES)
	{
		current_enemies++;
		enemies[current_enemies].eObject.status = ACTIVE;
	}
	return;
}
*/

// ***************************************************************************
// end of file
// ***************************************************************************

