// ***************************************************************************
// object
// ***************************************************************************

#pragma once
#include <vectrex.h>

// ---------------------------------------------------------------------------

enum status_t
{
	ACTIVE,
	INACTIVE,
};

// ---------------------------------------------------------------------------

struct object_t
{
	enum status_t status;	// object status
	int y;					// y coordinate byte
	int x;					// x coordinate byte
	int dy;					// delta y byte
	int dx;					// delta x byte
	int accel;				// acceleration
	int timeShooting;		//time spent until leaving
	int shoot;				//flag if currently shooting
};

// ---------------------------------------------------------------------------

void init_object(struct object_t* p, int speedY, int acc);
void init_objectc(struct object_t* p,int xPos, int speedY, int acc);
int move_object(struct object_t* p);

// ***************************************************************************
// end of file
// ***************************************************************************
