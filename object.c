// ***************************************************************************
// object
// ***************************************************************************

#include <vectrex.h>
#include "level.h"
#include "object.h"
#include "utils\collision.h"
#include "island.h"
#include "enemy.h"
#include "game.h"

// ---------------------------------------------------------------------------

void init_object(struct object_t* p, int speedY, int acc)
{
	p->status = ACTIVE;

	p->y = 120;
	p->x = 64 + (int)(Random() & 0b01111111);

	p->dy = speedY; 
	p->dx = 0;
	
	p->accel = acc;
}

// ---------------------------------------------------------------------------

void init_objectc(struct object_t* p,int xPos, int speedY, int acc)
{
	p->status = ACTIVE;

	p->y = 120;
	p->x = xPos;

	p->dy = speedY; 
	p->dx = 0;
	
	p->accel = acc;
}
// ---------------------------------------------------------------------------

int move_object(struct object_t* p)
{
	int pos = p->y - p->dy;
	int retval = 0;
	
	//difficulty
	if(p->dy < 3 && current_game.score >= 50) p->dy = 3; 
	if(p->dy < 4 && current_game.score >= 100) p->dy = 4;
	
	//out of bounds?
	if(pos < -120)
	{
		//spawn object at the top of the screen
		pos = 120;
		p->x =(int)((-100 + (int)Random()) & 0xFF);
		
		//check if spawning is colliding with other recently spawned objects
		for (unsigned int i = 0; i < current_islands; ++i)
		{
			if(islands[i].eObject.status == ACTIVE && islands[i].eObject.y >= 110)
			{
				if(check_collision(pos,p->x,islands[i].eObject.y,islands[i].eObject.x,10,10)) p->x =(int)((-100 +  (int)Random()) & 0xFF);
			}
		}
		for (unsigned int i = 0; i < current_enemies; ++i)
		{
			if(enemies[i].eObject.status == ACTIVE && enemies[i].eObject.y >= 110)
			{
				if(check_collision(pos,p->x,enemies[i].eObject.y,enemies[i].eObject.x,10,10)) p->x =(int)((-100 + (int)Random()) & 0xFF);
			}
		}
		
		//set spawn flag
		retval = 1;
		p->status = INACTIVE;
	}
	
	p->y = pos;
	
	return retval;
}

// ***************************************************************************
// end of file
// ***************************************************************************

