// ***************************************************************************
// enemy
// ***************************************************************************

#pragma once
#include "object.h"

// ---------------------------------------------------------------------------

#define MAX_ENEMIES 4

extern unsigned int current_enemies;


// ---------------------------------------------------------------------------


struct enemy_t
{
	struct object_t eObject;
	struct object_t canonball;
	long timeShooting;		//time spent until leaving
	int shoot;				// flag if shooting or not
};

 extern struct enemy_t enemies[MAX_ENEMIES];

// ---------------------------------------------------------------------------

void init_enemies(void);
void handle_enemies(void);
int move_canonball(struct object_t* p);
void draw_canonball(struct object_t* p);
void addEnemy(void);


// ***************************************************************************
// end of file
// ***************************************************************************
