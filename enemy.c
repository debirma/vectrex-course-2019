// ***************************************************************************
// enemy
// ***************************************************************************

#include <vectrex.h>
#include "utils/collision.h"
#include "utils/sound.h"
#include "utils/utils.h"
#include "utils/vector.h"
#include "enemy.h"
#include "player.h"
#include "tunes.h"
#include "game.h"

// ---------------------------------------------------------------------------

struct enemy_t enemies[] =
{
	{{.status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0},{.status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0} , .timeShooting=0, .shoot = 0 },
	{{.status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0},{.status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0}, .timeShooting=0, .shoot = 0 },
	{{.status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0},{.status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0}, .timeShooting=0, .shoot = 0 },
	{{.status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0},{.status = INACTIVE, .y = 0, .x = 0, .dy = 0, .dx = 0}, .timeShooting=0, .shoot = 0 },
};

unsigned int current_enemies = 2;

// ---------------------------------------------------------------------------

#undef SF
#define SF 11

const struct packet_t vectors_enemy[] =
{
	{MOVE, {  5 * SF, 0 * SF}},
	{DRAW, {  0 * SF, 1 * SF}},
	{DRAW, {  -1 * SF, 1 * SF}},
	{DRAW, {  -8 * SF, 0 * SF}},
	{DRAW, {  -1 * SF, -1 * SF}},
	{DRAW, {  -3 * SF, -1 * SF}},

	{DRAW, {  3 * SF, -1 * SF}},
	{DRAW, {  1 * SF, -1 * SF}},
	{DRAW, {  8 * SF, 0 * SF}},
	{DRAW, {  1 * SF, 1 * SF}},
	{DRAW, {  0 * SF, 1 * SF}},

	{STOP, { 0, 0}},
};

const struct packet_t vectors_canonball[] =
{
	{MOVE, {  1 * SF, 0 * SF}},
	{DRAW, {  0 * SF, 1 * SF}},
	{DRAW, {  -2 * SF, 0 * SF}},
	{DRAW, {  0 * SF, -2 * SF}},
	{DRAW, {  2 * SF, 0 * SF}},
	{DRAW, {  0 * SF, 1 * SF}},

	{STOP, { 0, 0}},
};

// ---------------------------------------------------------------------------

void draw_enemy(struct enemy_t* p)
{
	Reset0Ref();				// reset beam to center of screen
	dp_VIA_t1_cnt_lo = 0x7f;	// set scaling factor for positioning
	Moveto_d(p->eObject.y, p->eObject.x);		// move beam to object coordinates
	dp_VIA_t1_cnt_lo = 0x22;	// set scaling factor for drawing
	Draw_VLp(&vectors_enemy);	// draw vector list
	if(p->shoot)draw_canonball(&p->canonball);
}


// ---------------------------------------------------------------------------

void draw_canonball(struct object_t* p)
{
	Reset0Ref();				// reset beam to center of screen
	dp_VIA_t1_cnt_lo = 0x7f;	// set scaling factor for positioning
	Moveto_d(p->y, p->x);		// move beam to object coordinates
	dp_VIA_t1_cnt_lo = 0x22;	// set scaling factor for drawing
	Draw_VLp(&vectors_canonball);	// draw vector list
}

// ---------------------------------------------------------------------------

void check_enemy(struct enemy_t* p)
{
	if (check_collision(player.y, player.x, p->eObject.y, p->eObject.x, 40, 10))
	{
		play_explosion(&bang);
		player.status = DEAD;
	}
	
	if(p->shoot > 0)
	{
		if(check_collision(player.y, player.x, p->canonball.y, p->canonball.x,7,7))
		{
			play_explosion(&bang);
			player.status = DEAD;
		}
	}
}

// ---------------------------------------------------------------------------

void shoot(struct enemy_t* p)
{
	p->shoot = 1;
	init_objectc(&p->canonball,p->eObject.x, 5, 1);
}

// ---------------------------------------------------------------------------

void move_enemy(struct enemy_t* p)
{
	if(p->timeShooting <= 0)
	{
		if(move_object(&p->eObject))
		{
			p->timeShooting =  (int)(Random() << 4);
			p->eObject.status = ACTIVE;
			current_game.score++;
		}
	}
	else
	{
		if(!p->shoot)
		{
			shoot(p);
		}
	}
	p->timeShooting--;
	if(move_canonball(&p->canonball)) p->shoot = 0;
}

// ---------------------------------------------------------------------------

int move_canonball(struct object_t* p)
{
	return move_object(p);
}

// ---------------------------------------------------------------------------

void init_enemies(void)
{
	for (unsigned int i = 0; i < current_enemies; ++i)
	{
		init_object(&enemies[i].eObject, 2, 1);
		enemies[i].timeShooting = (int)(Random() << 4);
		enemies[i].shoot = 0;
	}
}
// ---------------------------------------------------------------------------

void handle_enemies(void)
{
	if(current_game.score == 20 && current_game.difficulty == 1)
	{
		addEnemy();
		current_game.difficulty++;
	}
	
	if(current_game.score == 35 && current_game.difficulty == 2)
	{
		addEnemy();
		current_game.difficulty++;
	}
	
	for (unsigned int i = 0; i < current_enemies; ++i)
	{
		if (enemies[i].eObject.status == ACTIVE)
		{
			draw_enemy(&enemies[i]);
			move_enemy(&enemies[i]);
			check_enemy(&enemies[i]);
		}
	}
}

// ---------------------------------------------------------------------------

void addEnemy(void)
{
	if(current_enemies < MAX_ENEMIES)
	{
		current_enemies++;
		init_object(&enemies[(current_enemies-1)].eObject, 2, 1);
		enemies[(current_enemies-1)].timeShooting = (int)(Random() << 4);
		enemies[(current_enemies-1)].shoot = 0;
		enemies[(current_enemies-1)].eObject.status = ACTIVE;
	}
	return;
}

// ***************************************************************************
// end of file
// ***************************************************************************

